terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

locals {
  region         = "us-east-1"
  db_name        = "demodbc4prof"
  vpc_name       = "demodb-c4-vpc"
  vpc_id         = "demodb-c4-vpc"
  public_subnets = ["10.0.101.0/24", "10.0.102.0/24"]

  common_tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
