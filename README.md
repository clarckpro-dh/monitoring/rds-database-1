# Ejercicio monitoreo con RDS

Recursos necesarios para esta práctica:
- Terraform instalado
    - link: https://learn.hashicorp.com/tutorials/terraform/install-cli
- Mysql-client instalado (mariadb).
    - link:https://mariadb.com/products/skysql/docs/clients/mariadb-clients/mariadb-client/
- Bash / VM linux.
- AWS-CLI.
    - link: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
- Acceso a la cuenta de AWS.
- ZIP con los archivos para la práctica 1 
    - https://drive.google.com/file/d/1DO4_fyDOKXA8s8UMMfc6q-6zr2gv0YkN/view?usp=sharing


```
cd terraform
terraform init -upgrade
terraform plan
terraform apply
```